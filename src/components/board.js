import "../App.css" ;
import {useEffect, useState} from 'react'
const Board = ()=>{

    const [tic,setTic] = useState({
        x1 : "",
        x2 : "",
        x3 : "",
        x4 : "",
        x5 : "",
        x6 : "",
        x7 : "",
        x8 : "",
        x9 : ""
    })

    const [value,setValue] = useState(true)

    useEffect(() => {
        if((tic.x1 !== "" && tic.x1  === tic.x2  && tic.x2 === tic.x3) || 
            (tic.x4 !== "" && tic.x4 === tic.x5 && tic.x5 === tic.x6) || 
            (tic.x7 !== "" && tic.x7 === tic.x8 && tic.x8 === tic.x9)||
            (tic.x1 !== "" &&  tic.x1 === tic.x4 && tic.x4 === tic.x7)  ||
            (tic.x2 !== "" && tic.x2 === tic.x5 && tic.x5 === tic.x8) ||
            (tic.x3 !== "" && tic.x3  === tic.x6  && tic.x6  === tic.x9) ||
            (tic.x1 !== "" && tic.x1 === tic.x5 && tic.x5  === tic.x9) || 
            (tic.x7 !== "" && tic.x7 === tic.x5 && tic.x5 === tic.x3)
            ) {
           console.log(`igra je zavrsena , pobednik je ${value ? 'O' : 'X'} `  )
        }
    }, [value])

    const changeTic = (fieldName) =>{
        console.log(tic[fieldName], "sdasadsa")
        if (tic[fieldName] === ""){
            if(value === true ) {
                setTic({...tic ,  [fieldName]: "X"})
            }
            else (
                setTic({...tic , [fieldName]: "O"})
            )
                setValue(!value)
            console.log("changeing tic value: " + fieldName);
        } 
     
       
        //     (tic.x1 === tic.x5 && tic.x5  === tic.x9) || (tic.x7 === tic.x5 && tic.x5 === tic.x3)
    }

    return (
        <div className="BoardStyle">
            <button disabled={tic.x1 !== ""} className="Square" onClick={() => changeTic("x1")}>{tic.x1}</button>
            <button disabled={tic.x2 !== ""} className="Square" onClick={() => changeTic("x2")}>{tic.x2}</button>
            <button disabled={tic.x3 !== ""} className="Square" onClick={() => changeTic("x3")}>{tic.x3}</button>
            <button disabled={tic.x4 !== ""} className="Square" onClick={() => changeTic("x4")}>{tic.x4}</button>
            <button disabled={tic.x5 !== ""} className="Square" onClick={() => changeTic("x5")}>{tic.x5}</button>
            <button disabled={tic.x6 !== ""} className="Square" onClick={() => changeTic("x6")}>{tic.x6}</button>
            <button disabled={tic.x7 !== ""} className="Square" onClick={() => changeTic("x7")}>{tic.x7}</button>
            <button disabled={tic.x8 !== ""} className="Square" onClick={() => changeTic("x8")}>{tic.x8}</button>
            <button disabled={tic.x9 !== ""} className="Square" onClick={() => changeTic("x9")}>{tic.x9}</button>
        </div>
    )  
     
}
export default Board